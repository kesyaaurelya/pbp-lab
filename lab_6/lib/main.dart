import 'package:flutter/material.dart';

void main() => runApp(ZonaHijau());

class ZonaHijau extends StatefulWidget {
  @override
  _ZonaHijauState createState() => _ZonaHijauState();
}

class _ZonaHijauState extends State<ZonaHijau> {

  String _jk="";

  TextEditingController controllerFrom = new TextEditingController();
  TextEditingController controllerMessage = new TextEditingController();

  void pilihJk(String value) {
    setState(() {
      _jk = value;
    });
  }

  void kirimdata(){
    AlertDialog alertDialog = new AlertDialog(
      content: new Container(
        height: 200.0,
        child: new Column(
          children: <Widget>[
            new Text("From : ${controllerFrom.text}"),
            new Text("Message : ${controllerMessage.text}"),
            new RaisedButton(
              child: new Text("OK"),
              onPressed: ()=>Navigator.pop(context),
            )
          ], // <Widget>[]
        ), // Column
      ), // Container
    ); // AlertDialog
    // showDialog(context: context, child: alertDialog);
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        leading: new Icon(Icons.list),
        title: new Text("Add FUFU"),
        backgroundColor: Colors.teal,

      ), // AppBar

      body: new ListView(
          children: <Widget>[
            new Container(
              padding: new EdgeInsets.all(10.0)
                // child: new Column(
                  children: <Widget>[
                  new TextField(
                  controller: controllerFrom,
                  decoration: new InputDecoration(
                      hintText: "From",
                      labelText: "Write your name",
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(20.0)
                      ) // OutlineInputBorder

                  ), // InputDecoration
                ), // TextField

                new Padding(padding: new EdgeInsets.only(top: 20.0),),

                // new RadioListTile(
                //     value: "Username",
                //     title: new Text("Username"),
                //     groupValue: _jk,
                //     onChanged: (String value) {
                //       pilihJk(value);
                //     },
                //     activeColor: Colors.grey,
                //     subtitle: new Text("choose this if you want to use your username")
                // ),
                // new RadioListTile(
                //     value: "Custom",
                //     title: new Text("Custom"),
                //     groupValue: _jk,
                //     onChanged: (String value) {
                //       pilihJk(value);
                //     },
                //     activeColor: Colors.grey,
                //     subtitle: new Text("choose this if you want to custom your name")
                // ),
                // new RadioListTile(
                //     value: "Anonymous",
                //     title: new Text("Anonymous"),
                //     groupValue: _jk,
                //     onChanged: (String value) {
                //       pilihJk(value);
                //     },
                //     activeColor: Colors.grey,
                //     subtitle: new Text("choose this if you want to be anonymous")
                // ),
                new Padding(padding: new EdgeInsets.only(top: 20.0),),
                new TextField(
                  controller: controllerMessage,
                  maxLines: 5,
                  decoration: new InputDecoration(
                      hintText: "Message",
                      labelText: "Write your message ...",
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(20.0)
                      ) // OutlineInputBorder

                  ), // InputDecoration
                ), // TextField
                new RaisedButton(
                  child: new Text("share"),
                  color: Colors.grey,
                  onPressed: (){ kirimdata();},
                )
              ], // <Widget>[]
            // ), // Column
            ), // Container
          ] // <Widget>[]
      ), // List View
    ); // Scaffold
  }
}