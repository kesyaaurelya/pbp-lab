import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Add FUFU",
    home: BelajarForm(),
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.teal[600],
        fontFamily: 'Sora',
        textTheme: const TextTheme(
          headline1: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
              color: Colors.black),
        )
    ),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String value = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add FUFU"),
        backgroundColor: Colors.teal[600],
      ),
      body: Center(
          child: Container(
            width: MediaQuery.of(context).size.width/2,
            child: Card(
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            "Add FUFU",
                            style: Theme.of(context).textTheme.headline1,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            decoration: new InputDecoration(
                              hintText: "Write your name",
                              labelText: "From",
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(20.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Nama tidak boleh kosong';
                              }
                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: TextFormField(
                            decoration: new InputDecoration(
                              hintText: "Write your message ...",
                              labelText: "Message",
                              border: OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(20.0)),
                            ),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "You haven't write any message";
                              }
                              return null;
                            },
                          ),
                        ),
                        RaisedButton(
                          child: Text(
                            "share",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: Colors.teal[600],
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              print("Successfully added!");
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          )
      ),
    );
  }
}