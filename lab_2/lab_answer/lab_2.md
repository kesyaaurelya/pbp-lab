1. Apakah perbedaan antara JSON dan XML?
   
   Baik JavaScript Object (JSON) maupun Extensive Markup Language (XML) merupakan sintaks yang digunakan untuk melakukan penyimpanan dan pertukaran data, namun keduanya memiliki banyak perbedaan. Pada dasarnya, XML merupakan suatu markup language, sedangkan JSON merupakan suatu format data.
   JSON merupakan format berbasis teks yang didasarkan pada bahasa pemrograman JavaScript yang dapat dibaca oleh manusia. Biasanya, JSON digunakan untuk mentransfer dan menyimpan data serta informasi secara terorganisir dan mudah untuk diakses. JSON merupakan format file yang lebih berorientasi pada data serta bersifat fleksibel dan sederhana, sehingga akan lebih sesuai digunakan untuk representasi atau pertukaran data antara aplikasi web dan layanan web agar dapat dimengerti oleh manusia. JSON lebih banyak digunakan untuk mengurus data-data sederhana dengan tipe data yang terbatas dan tanpa validasi. Ukuran file JSON sangat kecil, sehingga proses penguraian dan transfer data menjadi lebih cepat. Pada JSON, data akan disimpan dengan pasangan key-value.
   Sementara itu, XML merupakan teknologi yang lebih berorientasi pada dokumen, yang digunakan untuk menyandikan/mengkodekan data terstruktur dalam format yang dapat dibaca oleh manusia atau mesin. File XML digunakan untuk membuat format informasi umum serta menjadi sarana untuk membagikan format dan data yang digunakan pada World Wide Web, intranet, dan platform-platform lainnya yang menggunakan teks ASCII standar. XML biasanya baik digunakan ketika ingin menambahkan informasi tambahan pada teks biasa. XML akan terformat pada browser, dan dapat menampilkan tipe data yang lebih bervariasi daripada JSON. Terdapat tiga tipe file XML, yaitu XML yang merupakan standar format dari struktur file yang ada, XSL yang merupakan standar untuk memodifikasi data yang diimpor ataupun diekspor, dan XSD yang merupakan standar yang mendefinisikan struktur database dalam XML. XML memiliki tag untuk mendefinisikan elemen, sehingga XML memiliki ukuran file yang lebih besar yang berakibat pada penguraian dan proses transmisi data yang lebih lambat. Pada XML, data disimpan sebagai tree structure.
   
2. Apakah perbedaan antara HTML dan XML?

   Baik Extensible Markup Language (XML) maupun Hyper Text Markup Language (HTML) merupakan suatu markup language, namun keduanya memiliki beberapa perbedaan pada fungsi dan beberapa hal lainnya.
   HTML merupakan bahasa yang digunakan untuk membuat halaman dan aplikasi web dengan menerapkan tata letak dan konvensi pemformatan ke dokumen teks. HTML dirancang untuk mengatur format tampilan data, transfer dokumen berbasis web, dan mendeskripsikan struktur dari suatu halaman web. HTML didorong oleh format dan lebih difokuskan pada penyajian data. Selain itu, HTML memiliki tag yang cukup terbatas dan telah ditentukan sebelumnya.
   Sama halnya dengan HTML, XML juga digunakan untuk membuat halaman dan aplikasi web. Namun, XML tidak digunakan untuk menampilkan data, melainkan untuk mengangkut atau membawa data. Desain XML lebih ditujukan dan difokuskan pada kesederhanaan, umum, dan kegunaannya di internet. XML didorong oleh konten dan desainnya lebih berfokus pada transfer data dan dokumen. Berbeda dengan HTML, tag XML biasanya dapat dikembangkan kembali dan tidak ditentukan sebelumnya. XML juga menyediakan framework untuk menentukan markup language.
   
Sumber:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://www.django-rest-framework.org/api-guide/serializers/
https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html
https://www.it-swarm-id.com/id/xml/perbandingan-json-dan-xml/972071942/
https://askanydifference.com/id/perbedaan-antara-json-dan-xml/
https://www.petanikode.com/json-pemula/
https://www.niagahoster.co.id/blog/xml/
https://www.nesabamedia.com/pengertian-json/