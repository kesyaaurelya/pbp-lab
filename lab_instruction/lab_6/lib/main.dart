import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Sign Up",
    home: BelajarForm(),
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.teal[600],
        fontFamily: 'Sora',
        textTheme: const TextTheme(
          headline1: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        )
      ),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String value = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sign Up ZonaHijau"),
        backgroundColor: Colors.teal[600],
      ),
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.width/2,
          child: Card(
            child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "SIGN UP AN ACCOUNT",
                        style: Theme.of(context).textTheme.headline1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "contoh: Susilo Bambang",
                          labelText: "Username ...",
                          icon: Icon(Icons.people),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Nama tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        decoration: new InputDecoration(
                          labelText: "Enter password",
                          icon: Icon(Icons.vpn_key_rounded),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Password tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        decoration: new InputDecoration(
                          labelText: "Re-enter Password...",
                          icon: Icon(Icons.vpn_key_rounded),
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'tidak boleh kosong';
                          }
                          return null;
                        },
                      ),
                    ),
                    RaisedButton(
                      child: Text(
                        "Sign Up",
                        style: TextStyle(color: Colors.white),
                      ),
                      color: Colors.teal[600],
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          print("Sign in berhasill");
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        )
      ),
    );
  }
}
================================================================
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab_7/widget/textfield_border_widget.dart';
import 'package:lab_7/widget/textfield_focus_widget.dart';
import 'package:lab_7/widget/textfield_general_widget.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static final String title = 'Add FUFU';

  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Add FUFU',
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.teal[600]
      // fontFamily: '',
      // textTheme: const TextTheme(
      //   headline1: TextStyle(
      //     fontSize: 25,
      //     fontWeight: FontWeight.bold,
      //     color: Colors.black
      //   ),
      // )
    ),
    // home: MainPage(title: title),
  );
}

class MainPage extends StatefulWidget {
  final String title = 'Add FUFU';

  // const MainPage({
  // @required this.title,
  // });

  void kirimdata(){

  }

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int index = 0;
  final fromController = TextEditingController();
  final messageController = TextEditingController();
  String message = '';

  void kirimdata(){

  }

  @override
  void initState() {
    super.initState();

    fromController.addListener(() => setState(() {}));
  }

  @override
  void dispose() {
    fromController.dispose();
    messageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add FUFU"),
        backgroundColor: Colors.teal[600],
      ),
      // body:
      body: ListView(
        children: <Widget>[
          new Container(
              padding: new EdgeInsets.all(32),
              // children: [
              // buildText('From'),
              child: new TextField(
                controller: fromController,
                decoration: new InputDecoration(
                  hintText: "From",
                  labelText: "Write your name",
                  enabledBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(20),
                    borderSide: new BorderSide(color: Colors.black, width: 2),
                  ),
                  focusedBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(20),
                    borderSide: new BorderSide(color: Colors.teal, width: 2),
                  ),
                ),
              ),

              new Padding(padding: new EdgeInsets.only(top: 20.0),),
              new TextField(
                onChanged: (value) => setState(() => this.message = value),
                onSubmitted: (value) => setState(() => this.message = value),
                maxLines: 10,
                decoration: new InputDecoration(
                  hintText: "Message",
                  labelText: "Write your message ...",
                  enabledBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(20),
                    borderSide: new BorderSide(color: Colors.black, width: 2),
                  ),
                  focusedBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(20),
                    borderSide: new BorderSide(color: Colors.teal, width: 2),
                  ),
                ),
              ),
              new RaisedButton(
                child: new Text("share"),
                color: Colors.grey,
                onPressed: (){ kirimdata();},
              )
          ),
        ],
      ),
    );
    //   body: buildPages(),
    //   bottomNavigationBar: BottomNavigationBar(
    //     currentIndex: index,
    //     selectedItemColor: Colors.teal[600],
    //     items: [
    //       BottomNavigationBarItem(
    //         icon: Text('TextField'),
    //         title: Text('General'),
    //       ),
    //       BottomNavigationBarItem(
    //         icon: Text('TextField'),
    //         title: Text('Borders'),
    //       ),
    //       BottomNavigationBarItem(
    //         icon: Text('TextField'),
    //         title: Text('Focus'),
    //       ),
    //     ],
    //     onTap: (int index) => setState(() => this.index = index),
    //   ),
    // );
    //
    // Widget buildPages() {
    //   switch (index) {
    //     case 0:
    //       return TextfieldGeneralWidget();
    //     case 1:
    //       return TextfieldBorderWidget();
    //     case 2:
    //       return TextfieldFocusWidget();
    //     default:
    //       return Container();
    //   }
    // }
  }
}